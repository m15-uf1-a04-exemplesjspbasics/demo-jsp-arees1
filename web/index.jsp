<%@page import="index.Validation"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="index.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP DEMOS - CÀLCUL ÀREES.</title>
    </head>
    <body>
        <h1>JSP DEMOS - CÀLCUL ÀREA D'UN RECTANGLE</h1>
        <form method="post" action="index.jsp">
            <label for="base">Base (en cms):</label>
            <input type="text" name="base" id="base" placeholder="10"><br/>
            <label for="alsada">Alçada (en cms)</label>
            <input type="text" name="alsada" id="alsada" placeholder="10"><br/>
            <input type="submit" name="ok" value="Enviar"/>
        </form>  
       
        <%
            //si ha clicat o no al botó del formulari
           if(request.getParameter("ok")!=null) {
              
              double base, alsada;
              //debería validar los valores de mis cajas
              //según lo entrado, me debería salir o no un resultado
              //crear una clase de validacion
              
              base=Validation.validaDouble(request.getParameter("base"));
              alsada=Validation.validaDouble(request.getParameter("alsada"));
             
              if(base <0 || alsada <0){
                  out.println("Has d'introduir números positius");
              }else{
                CalculaArees indice = new CalculaArees(base,alsada);

                out.println("L'àrea és de: <br/>");
                out.println(String.format("%.2f", indice.calculaArea()));
              }
           }
           
        %>
        
    </body>
</html>
