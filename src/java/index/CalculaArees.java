
package index;

/**
 * Aquesta classe calcula l'index de massa corporal d'un individu
 * @author alumne
 * @version 1.0, 2018-11-14
 */
public class CalculaArees {
    
    private double base;
    private double alsada;
    private double area;
    private double perimetre;
    

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimetre() {
        return perimetre;
    }

    public void setPerimetre(double perimetre) {
        this.perimetre = perimetre;
    }
    
    public CalculaArees(double base, double alsada) {
        this.base = base;
        this.alsada = alsada;
    }

    public double getAlsada() {
        return this.alsada;
    }

    public void setAlsada(double alsada) {
        this.alsada = alsada;
    }
    
    /**Mètode que fa el càlcul de l'area; a partir de base i l'alçada
     * 
     * @return index de massa corporal
     */
    public double calculaArea(){
        return alsada * base;
    }
    
    /**Mètode que fa el càlcul del perimetre; a partir de base i l'alçada
     * 
     * @return index de massa corporal
     */
    public double calculaPerimetre(){
        return 0.0 ;
    }
    
    
}
